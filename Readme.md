### Build the app

Clone and install the required packages
```
git clone https://bitbucket.org/utiq/rhythm-shoes-app
cd rhythm-shoes-app
npm install
```

Add the ios platform
```
ionic platform add ios
```

This app was built with Ionic 3.5.3, to build the app just execute this command.

```
ionic build ios
```
You can run directly in the device using the command below, most of the cases don't work because you have to correctly install [ios-deploy](https://github.com/phonegap/ios-deploy)
```
ionic run ios --device
```
If it doesn't work, just execute ```ionic build ios```, then open the XCode project located in the folder: platforms > ios > rhythm-shoes-app.xcodeproj
and then press run on XCode

You can also run in the browser with:
```
ionic serve
```
In this case it doesn't work because it needs bluetooth connection from the device. But it works to work on the styling without having to wait for the app to build.

### Arduino configuration

Bluetooth discovery is hard-coded so replace your device UUID in this function:

src > app > shared > shoes-service.ts

```
getDeviceId(foot: string): string {
  let deviceId = '';
  if (foot == 'left') {
    deviceId = "4C0994F9-3B27-4BAB-9D21-5E68E21BE49D"; // Left Shoe
  } else {
    deviceId = "A15CD811-4F7E-4075-B925-31B42FF58429"; // Right Shoe
  }
  return deviceId;
}
```

### Ionic cli
$ ionic generate
```
$ ionic generate
$ ionic generate component
$ ionic generate directive
$ ionic generate page
$ ionic generate pipe
$ ionic generate provider
$ ionic generate tabs
$ ionic generate component foo
$ ionic generate page Login
$ ionic generate pipe MyFilterPipe
```
More in the docs: https://ionicframework.com/docs/cli/generate/
