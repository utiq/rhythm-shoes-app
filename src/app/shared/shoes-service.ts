import { Injectable } from '@angular/core';
import { BLE } from "ionic-native";
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ShoesService {
  serviceUuid: string = "19B10010-E8F2-537E-4F6C-D104768A1214";
  vibrateCharUuid: string = "19B10011-E8F2-537E-4F6C-D104768A1214";
  stepCharUuid: string = "19B10012-E8F2-537E-4F6C-D104768A1214";
  private stepMessage = new Subject<any>();

  constructor() { }

  getDeviceId(foot: string): string {
    let deviceId = '';
    if (foot == 'left') {
      deviceId = "4C0994F9-3B27-4BAB-9D21-5E68E21BE49D"; // Left Shoe
    } else {
      deviceId = "A15CD811-4F7E-4075-B925-31B42FF58429"; // Right Shoe
    }
    return deviceId;
  }

  vibrate(foot: string, position: number): void {
    console.log(foot, position);
    BLE.write(this.getDeviceId(foot), this.serviceUuid, this.vibrateCharUuid, this.stringToBytes(position.toString())).then((success: any) => {
      console.log('Enter success', success);
    }).catch((error: any) => {
      console.log('Enter error', error);
    }).then(() => {
      console.log('Enter then');
		});
  }

  listenForSteps(foot: string): void {
    BLE.startNotification(this.getDeviceId(foot), this.serviceUuid, this.stepCharUuid).subscribe(buffer => {
        this.stepMessage.next(foot);
        },
      error => {console.log("Error Notification" + JSON.stringify(error));
    })
  }

  getStepMessage(): Observable<any> {
    return this.stepMessage.asObservable();
  }

  // ASCII only
  stringToBytes(string): any {
    var array = new Uint8Array(string.length);
    for (var i = 0, l = string.length; i < l; i++) {
      array[i] = string.charCodeAt(i);
    }
    return array.buffer;
  }

  // ASCII only
  bytesToString(buffer): string {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
  }

}
