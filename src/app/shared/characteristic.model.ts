export class Characteristic {

  constructor(public uuid: string,
              public properties: Array<string>,
              public isNotifying: boolean,
              public service: string) {}
}
