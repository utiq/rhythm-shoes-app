import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { VideoClassPage } from '../pages/video-class/video-class';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { ShoesService } from './shared/shoes-service'

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    VideoClassPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    VideoClassPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ScreenOrientation,
    ShoesService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
