import { Component, ViewChild, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { Characteristic } from '../../app/shared/characteristic.model';
import { Device } from '../../app/shared/device.model';
import { BLE } from "ionic-native";
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { ShoesService } from '../../app/shared/shoes-service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'page-device',
  templateUrl: 'video-class.html',
})

export class VideoClassPage implements OnInit, OnDestroy {
  connecting = false;
  leftShoeClass: string;
  rightShoeClass: string;
  steps: Array<any>;
  currentStep: number;
  subscription: Subscription;
  stepsPerformed: number;

  constructor(public navParams: NavParams,
              public nav: NavController,
              public platform: Platform,
              private screenOrientation: ScreenOrientation,
              public shoesService: ShoesService,
              private cd:ChangeDetectorRef) {

    this.leftShoeClass = '';
    this.rightShoeClass = '';
    this.currentStep = 0;
    this.stepsPerformed = 0;
    this.steps = [
      { foot: 'right',
        time: 27,
        position: 'middle-foward',
        number: 1,
        vibrator: 1
      },
      { foot: 'left',
        time: 28,
        position: 'full-foward',
        number: 2,
        vibrator: 1
      },
      { foot: 'right',
        time: 28.20,
        position: 'up',
        number: 3,
        vibrator: 5
      },
      { foot: 'left',
        time: 29.20,
        position: 'middle-backward',
        number: 5,
        vibrator: 3
      },
      { foot: 'right',
        time: 30.10,
        position: 'full-backward',
        number: 6,
        vibrator: 3
      },
      { foot: 'left',
        time: 31.02,
        position: 'up',
        number: 7,
        vibrator: 5
      },
      // Second
      { foot: 'right',
        time: 31.27,
        position: 'middle-foward',
        number: 1,
        vibrator: 1
      },
      { foot: 'left',
        time: 32.19,
        position: 'full-foward',
        number: 2,
        vibrator: 1
      },
      { foot: 'right',
        time: 33,
        position: 'up',
        number: 3,
        vibrator: 5
      },
      { foot: 'left',
        time: 33.28,
        position: 'middle-backward',
        number: 5,
        vibrator: 3
      },
      { foot: 'right',
        time: 34.00,
        position: 'full-backward',
        number: 6,
        vibrator: 3
      },
      { foot: 'left',
        time: 34.23,
        position: 'up',
        number: 7,
        vibrator: 5
      },
      // Third
      { foot: 'right',
        time: 35.18,
        position: 'middle-foward',
        number: 1,
        vibrator: 1
      },
      { foot: 'left',
        time: 36.06,
        position: 'full-foward',
        number: 2,
        vibrator: 1
      },
      { foot: 'right',
        time: 36.21,
        position: 'up',
        number: 3,
        vibrator: 5
      },
      { foot: 'left',
        time: 37.10,
        position: 'middle-backward',
        number: 5,
        vibrator: 3
      },
      { foot: 'right',
        time: 37.48,
        position: 'full-backward',
        number: 6,
        vibrator: 3
      },
      { foot: 'left',
        time: 38.14,
        position: 'up',
        number: 7,
        vibrator: 5
      },
      // Fourth
      { foot: 'right',
        time: 39.01,
        position: 'middle-foward',
        number: 1,
        vibrator: 1
      },
      { foot: 'left',
        time: 39.22,
        position: 'full-foward',
        number: 2,
        vibrator: 1
      },
      { foot: 'right',
        time: 40.10,
        position: 'up',
        number: 3,
        vibrator: 5
      },
      { foot: 'left',
        time: 41.00,
        position: 'middle-backward',
        number: 5,
        vibrator: 3
      },
      { foot: 'right',
        time: 41.25,
        position: 'full-backward',
        number: 6,
        vibrator: 3
      },
      { foot: 'left',
        time: 42.5,
        position: 'up',
        number: 7,
        vibrator: 5
      },
    ]
  }

  ngOnInit() {

    this.platform.ready().then(
    () => {
        if (!this.platform.is('core')) {
          this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
          this.listenForSteps();
        }
      }
    );

    this.subscription = this.shoesService.getStepMessage().subscribe(message => {
      console.log('message:', message)
      // We can diferenciate if is the left foot or the right foot
      this.stepsPerformed++;
      this.cd.detectChanges();
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  listenForSteps() {
    this.shoesService.listenForSteps('left');
    // this.shoesService.listenForSteps('right');
  }

  playVideo() {
    var vidURL = "assets/videos/basic-salsa-01.mp4";
    var myVideo = document.getElementsByTagName('video')[0];
    myVideo.src = vidURL;
    myVideo.load();
    myVideo.play();

    var self = this;
    myVideo.addEventListener("timeupdate", function() {
      // console.log(myVideo.currentTime)
      self.steps.forEach(step => {
        if(myVideo.currentTime >= step.time - 0.5 && myVideo.currentTime <= step.time + 0.5) {
          self.currentStep = step.number;
          if (step.foot == 'left') {
            self.leftShoeClass = step.position;
            self.shoesService.vibrate(step.foot, step.vibrator); // this should be out of this if
          } else {
            self.rightShoeClass = step.position;
          }
        }
      });
    }, true);
  }

  moveUp(): void {
    if (this.steps[this.currentStep].foot == 'left') {
      this.leftShoeClass = this.steps[this.currentStep].position;
    } else {
      this.rightShoeClass = this.steps[this.currentStep].position;
    }
    this.currentStep++;
  }

}
