import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { VideoClassPage } from '../video-class/video-class';
import { BLE } from "ionic-native";
import { ShoesService } from '../../app/shared/shoes-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  isScanning: Boolean;
  leftShoeIsConnected: Boolean;
  rightShoeIsConnected: Boolean;

  constructor(public nav: NavController,
              public shoesService: ShoesService,
              public platform: Platform,
              private cd:ChangeDetectorRef) {

    this.leftShoeIsConnected = false;
    this.rightShoeIsConnected = false;
    this.isScanning = false;
  }

  ngOnInit() {
    this.platform.ready().then((readySource) => {
        if (!this.platform.is('core')) {
          this.startScanning();
        }
      }
    );
  }

  connectShoe(foot: string): void {
    // Disabling right foot connection for test purposes
    if (foot == 'left') {
      BLE.connect(this.shoesService.getDeviceId(foot)).subscribe(peripheralData => {
        console.log(peripheralData)
        if (foot == 'left') {
          this.leftShoeIsConnected = true;
        } else {
          this.rightShoeIsConnected = true;
        }
        this.cd.detectChanges();
        // this.connecting = false;
      },
        peripheralData => {
          console.log('disconnected');
        });
    } else {
      this.rightShoeIsConnected = true;
    }

  }

  vibrate(foot: string, position: number): void {
    this.shoesService.vibrate(foot, position);
  }

  startClass(): void {
    this.nav.push(VideoClassPage, {});
  }

  startScanning() {
    console.log("Scanning Started");
    BLE.startScan([]).subscribe(device => {
      console.log(device);
    });

    setTimeout(() => {
      BLE.stopScan().then(() => {
        console.log("Scanning has stopped");
      });
    }, 2000);

  }

}
